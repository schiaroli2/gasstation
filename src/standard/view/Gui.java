package standard.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Gui extends Application {

    /**
     * Main.
     *
     * @param args command line arguments
     */
    public static void main(final String[] args) {
        launch(args);
    }

    /**
     * Start the gui.
     *
     * @param stage Stage where the gui is designed
     * @throws Exception in fxml file load
     */
    @Override
    public void start(final Stage stage) throws Exception {
        final Parent root = FXMLLoader.load(getClass().getResource("Gui.fxml"));
        final Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

}
