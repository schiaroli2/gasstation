package standard.control;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * Inventory manager.
 * 
 */
public class ProductPlacement {

    @FXML
    private TextField barcode, nome, produttore, peso, quantita, prezzo, scadenza, promozione;
    @FXML
    private Button insert;

    private String barcodeText, name, trade, weigth, quantity, price, expire, promo;

    /**
     * Click handle.
     */
    public void insertClick() {
        /*
         * Se il codice a barre è stato inserito non leggo altri campi e ricavo tutti i
         * valori dal database (solo nel caso sia già presente
         */
        if (barcode.getText().isBlank()) {
            /*
             * Segnalo errore
             */
        } else {
            // Barcode presente nel database
            if (false) {
                // Prendo valori dal database
            } else {
                // Creo una nuova entry
                barcodeText = barcode.getText();
                name = nome.getText();
                trade = produttore.getText();
                weigth = peso.getText();
                quantity = quantita.getText();
                price = prezzo.getText();
                expire = scadenza.getText();
                promo = promozione.getText();
            }
        }
    }

}
