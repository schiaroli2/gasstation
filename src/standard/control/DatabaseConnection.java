package standard.control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/*
        TO DO:
        Per prima cosa fai in modo che la connessione al database si apra e
        si chiuda insieme all'applicazione, solo se convenieente.
 */
/**
 * Database initializer.
 *
 */
public class DatabaseConnection {
    private final List<String> query = new ArrayList<String>();
    private Connection con;

    private int errorCode;
    private int size;

    /**
     * Error code getter.
     *
     * @return the error code value
     */
    public int getErrorCode() {
        return errorCode;
    }

    /*
     * ArrayList getPrices() throws SQLException{ return
     * getRow(4,"PrezziGiornata",null); }
     */

    /**
     * Get prices for the indicated day.
     *
     * @param ld day choosen
     * @return an array of string rappresenting the prices
     * @throws SQLException
     */
    List<String> getPricesDay(final LocalDate ld) throws SQLException {
        return getRow(4, "PrezziGiornata", ld);
    }

    private List<String> getRow(int num, final String table, final LocalDate ld) throws SQLException {
        final Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet rs = null;
        if (ld != null) {
            rs = stmt.executeQuery("SELECT * FROM `" + table + "`" + " WHERE DATA = '" + ld.getYear() + "-"
                    + ld.getMonthValue() + "-" + ld.getDayOfMonth() + "'");
            // Questa routine legge le prime N colonne e le aggiunge ad una lista, ad ogni
            // next() scorre di una riga
            try {

                rs.last();
                size = rs.getRow();
                rs.beforeFirst();
            } catch (final Exception ex) {
                ex.printStackTrace();

            }
            if (size == 0) {
                System.out.println("Errore, nessun prezzo nella data selezionata");
                setErrorCode(2);
            } else {
                rs.next();
                for (int i = 1; i < num + 1; i++) {
                    query.add(rs.getString(i));
                }
                con.close();
            }
        } else {
            System.out.println("No date selected"); // rs = stmt.executeQuery("SELECT * FROM `"+table+"`");
            setErrorCode(1);

        }
        if (!rs.isClosed()) {
            rs.close();
        }
        if (!stmt.isClosed()) {
            stmt.close();
        }
        if (query.isEmpty()) {
            return null;
        } else {
            return query;
        }

    }

    /**
     * Error code setter.
     *
     * @param newErrorCode new code to be setted
     */
    public void setErrorCode(final int newErrorCode) {
        errorCode = newErrorCode;
    }

    /**
     * Initialize an sql connection between Gui application and DB.
     *
     * @throws SQLException if there are error in db connection
     */
    public void start() throws SQLException {
        final String username = "user";
        final String password = "password";
        final String conn = "jdbc:mysql://localhost/gas_station";
        try {
            con = DriverManager.getConnection(conn, username, password);
            System.out.println("Correctly connected to the databse");
        } catch (final Exception e) {
            System.out.println("Error trying to connect to the database: " + e.toString());
        }
    }
}
