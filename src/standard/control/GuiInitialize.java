package standard.control;

import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class GuiInitialize implements Initializable {

    @FXML
    private Tab carburanti;
    @FXML
    private Label advice, benzina, diesel, benzinaPiu, dieselPiu, errorLabel;
    @FXML
    private TextField benzinaField, benzinaPiuField, dieselField, dieselPiuField;
    @FXML
    private Button loadButton;
    @FXML
    private DatePicker datePicker;
    @FXML
    private AnchorPane anchr;

    @FXML
    private StackedBarChart serbatoio;

    @FXML
    public LocalDate getDate() {
        final LocalDate ld = datePicker.getValue();
        System.out.println(ld);
        return ld;
    }

    @FXML
    public void handleClick() throws SQLException {
        errorLabel.setDisable(true);
        final DatabaseConnection dbconn = new DatabaseConnection();
        List<String> prices;
        final LocalDate ld = getDate();
        dbconn.start();
        prices = dbconn.getPricesDay(ld);
        if (dbconn.getErrorCode() == 1) {
            errorLabel.setDisable(false);
            errorLabel.setText("Seleziona prima una data");
        } else if (dbconn.getErrorCode() == 2) {
            errorLabel.setDisable(false);
            errorLabel.setText("Nessuna prezzo nella data selezionata");
        }
        if (prices == null) {
            System.out.println("Prices returned NULL");
        } else { // Label update
            errorLabel.setText("");
            benzinaField.setText(prices.get(0));
            benzinaPiuField.setText(prices.get(1));
            dieselField.setText(prices.get(2));
            dieselPiuField.setText(prices.get(3));
        }

        final XYChart.Series dataSeries1 = new XYChart.Series();
        dataSeries1.setName("Desktop");

        dataSeries1.getData().add(new XYChart.Data("2014", 567));
        dataSeries1.getData().add(new XYChart.Data("2015", 540));

        serbatoio.getData().add(dataSeries1);

        final XYChart.Series dataSeries2 = new XYChart.Series();
        dataSeries2.setName("Phone");

        dataSeries2.getData().add(new XYChart.Data("2014", 65));
        dataSeries2.getData().add(new XYChart.Data("2015", 120));

        serbatoio.getData().add(dataSeries2);

        final XYChart.Series dataSeries3 = new XYChart.Series();
        dataSeries3.setName("Tablet");

        dataSeries3.getData().add(new XYChart.Data("2014", 23));
        dataSeries3.getData().add(new XYChart.Data("2015", 36));

        serbatoio.getData().add(dataSeries3);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        assert anchr != null : "fx:id=\"anchr\" was not injected: check your FXML file 'Gui.fxml'.";
    }

}